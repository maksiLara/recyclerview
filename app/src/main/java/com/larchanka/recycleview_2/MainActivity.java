package com.larchanka.recycleview_2;


import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public RecyclerView recyclerView;

    private LinearLayoutManager linearLayoutManagerHorizontal;
    private LinearLayoutManager linearLayoutManagerVertical;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycleView);

        linearLayoutManagerVertical = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManagerVertical);

        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.addAll(ModelItem.getModelItem());
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerHolder> {
        ArrayList<ModelItem> modelItems = new ArrayList<>();

        public void addAll(List<ModelItem> item) {
            int position = getItemCount();
            modelItems.addAll(item);
            notifyItemRangeInserted(position, modelItems.size());
        }

        @Override
        public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
            return new RecyclerHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerHolder holder, int position) {
            holder.bindItem(modelItems.get(position));
        }

        @Override
        public int getItemCount() {
            return modelItems.size();
        }
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        public RecyclerHolder(final View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image);
        }

        public void bindItem(ModelItem modelItem) {
            imageView.setImageBitmap(BitmapFactory.decodeResource(itemView.getResources(), modelItem.getImgId()));
            textView.setText(modelItem.getAuthor());
        }
    }

}