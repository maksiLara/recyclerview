package com.larchanka.recycleview_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Максим on 13.12.2017.
 */

public class ModelItem {
    private String author;
    private int imgId;

    public ModelItem(String author, int imgId) {
        this.author = author;
        this.imgId = imgId;
    }

    public String getAuthor() {
        return author;
    }

    public int getImgId() {
        return imgId;
    }

    public static List<ModelItem> getModelItem(){
        ArrayList<ModelItem> modelItems = new ArrayList<>();
        modelItems.add(new ModelItem("Картинка",R.drawable.q));
        modelItems.add(new ModelItem("Картинка",R.drawable.w));
        modelItems.add(new ModelItem("Картинка",R.drawable.e));
        modelItems.add(new ModelItem("Картинка",R.drawable.r));
        modelItems.add(new ModelItem("Картинка",R.drawable.q));
        modelItems.add(new ModelItem("Картинка",R.drawable.w));
        modelItems.add(new ModelItem("Картинка",R.drawable.e));
        return modelItems;
    }
}
